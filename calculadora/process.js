const {createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador: null,
            numeroAtual: null,
            numeroAnterior: null,
            tamanhoDisplay: 10,
            tamanhoLetra: 50 + "px",
        };
    },//fechamento data

    methods:{
        numero(valor){
            this.displayResponsivo();
            if(this.display == "0"){
                this.display = valor.toString();
            }
            


            else{

                if(this.operador == "="){
                    this.display = '';
                    this.operdor = null;
                }
               // this.display = this.display + valor.toString();
                //formula resumida:
                this.display += valor.toString();
            }
        },//fechamento numero

        decimal(){
            if(!this.display.includes(".")){
                //this.display = this.display + ".";

            //formuola resumida:
            this.display += ".";
            }
        },

        clear(){
            this.display= "0";
            this.numeroAtual = null;
            this.numeroAnterior = null;
            this.operador = null;
            this.displayResponsivo();
        },//fechamento clear

        operacoes(operacao){
            if(this.operador != null){
                const displayAtual = parseFloat(this.display);
                switch(this.operador){
                    case "+":
                        this.display = (this.numeroAtual + displayAtual).toString();
                        break;
                    case"-":
                        this.display = (this.numeroAtual - displayAtual).toString();
                        break;
                    case "*":
                        this.display = (this.numeroAtual * displayAtual).toString();
                        break;
                    case"/":
                        this.display = (this.numeroAtual / displayAtual).toString();
                        break;


                }//fim do swhitch
                
                this.numeroAnterior = this.numeroAtual;
                this.numeroAtual = null;
                
                if(this.display == "NaN"){
                    this.display = "Operação invalida!";
                }
                else if (this.display == "infinity"){
                    this.display = "Número Infinito!";
                }

            }//fim do if

            if(operacao != "="){

                this.operador = operacao;
                this.numeroAtual = parseFloat(this.display);
                this.display = "0";
            }
            else{
                this.operador = operacao;

                if(this.display.includes(".")){
                    const displayAtual = parseFloat(this,display);
                    this.display = (displayAtual.toFixed(2)).toString();
                }
             }
             this.displayResponsivo();
        },//fechaento operacoes

        displayResponsivo(){
            if(this.display.length >= 31){
                this.tamanhoLetra = 14 + "px";
            }
            else if(this.display.length >= 20){
                this.tamanhoLetra = 20 + "px";
            }
            else if(this.display.length >=12){
                this.tamanhoLetra = 30 + "px";
            }
            else{
                this.tamanhoLetra = 50 + "px";
            }

            


        },//fechamento displayResponsivo
    },//fechamento methods

}).mount("#app")//fechamento createApp