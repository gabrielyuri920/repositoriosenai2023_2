const{createApp} = Vue;

createApp({
    data(){
        return{
          products:[
            {
                id: 1,
                name:"Tênis",
                description:"Um par de tênis confortável para esportes",
                price: 129.99,
                image: "./imagens/imgTenis.jpg",
            },//fechamento intem 1
            {
                id: 2,
                name: "Botas",
                description: "botas elegantes para qualquer ocasião",
                price: 199.99,
                image: "./imagens/imgBotas.jpg",
            },//fechamento item 2
            {
                id: 3,
                name:"Sapatos",
                description: "Sapatos clássicos para um visual sofisticado!",
                price: 149.99,
                image:"./imagens/imgSapatos.jpg",
            },//fechamento item 3
            {
                id: 4,
                name: "sandálias",
                description: "Sandálias confortáveis para seus pés!",
                price: 69.99,
                image: "./imagens/imgSandalias.jpg",
            },//fechamento item 4
          ],//fechamento products  
          currentProduct: {}, //produto atual
          cart: [],
          
        };//fechamento return
    },//fechamento data

    mounted(){
        window.addEventListener("hashchange", this.updateProduct);
        this.updateProduct();
    },//fechamento mounted

    //funçao vue para retonar resultados especificos de um bloco programado
    computed:{
        cartItemCount(){
            return this.cart.length;

        },//fechamento cartitemcount

        cartTotal(){
            return this.cart.reduce((total, product) => total + product.price, 0);
        },//fechamento cartotal

    },//fechamento computed

    methods:{
        updateProduct(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};

        },//fechamento updateProduct

        addToCart(product){
            this.cart.push(product);
        },//fechamento addtocart

        removeFromCart(product){
            const index = this.cart.indexOf(product);
            if(index != -1){
                this.cart.splice(index,1);
            }
        },//fechamento remove

    },//fechamento methods
}).mount("#app");//fechamento createApp